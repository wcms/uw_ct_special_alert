<?php

/**
 * @file
 * uw_ct_special_alert.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_special_alert_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_special_alert content'.
  $permissions['create uw_special_alert content'] = array(
    'name' => 'create uw_special_alert content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_special_alert content'.
  $permissions['delete any uw_special_alert content'] = array(
    'name' => 'delete any uw_special_alert content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_special_alert content'.
  $permissions['delete own uw_special_alert content'] = array(
    'name' => 'delete own uw_special_alert content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_special_alert content'.
  $permissions['edit any uw_special_alert content'] = array(
    'name' => 'edit any uw_special_alert content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_special_alert content'.
  $permissions['edit own uw_special_alert content'] = array(
    'name' => 'edit own uw_special_alert content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_special_alert revision log entry'.
  $permissions['enter uw_special_alert revision log entry'] = array(
    'name' => 'enter uw_special_alert revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_special_alert authored by option'.
  $permissions['override uw_special_alert authored by option'] = array(
    'name' => 'override uw_special_alert authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_special_alert authored on option'.
  $permissions['override uw_special_alert authored on option'] = array(
    'name' => 'override uw_special_alert authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_special_alert promote to front page option'.
  $permissions['override uw_special_alert promote to front page option'] = array(
    'name' => 'override uw_special_alert promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_special_alert published option'.
  $permissions['override uw_special_alert published option'] = array(
    'name' => 'override uw_special_alert published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_special_alert revision option'.
  $permissions['override uw_special_alert revision option'] = array(
    'name' => 'override uw_special_alert revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_special_alert sticky option'.
  $permissions['override uw_special_alert sticky option'] = array(
    'name' => 'override uw_special_alert sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
      'special alerter' => 'special alerter',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
