<?php

/**
 * @file
 * uw_ct_special_alert.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_special_alert_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_special_alert_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_special_alert_node_info() {
  $items = array(
    'uw_special_alert' => array(
      'name' => t('Special alert'),
      'base' => 'node_content',
      'description' => t('A special alert displays at the top of pages you specify, surrounded by a red border. It is used to convey urgently important information.'),
      'has_title' => '1',
      'title_label' => t('Title (admin use only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
