<?php

/**
 * @file
 * uw_ct_special_alert.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_special_alert_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'special_alert';
  $context->description = '';
  $context->tag = 'content';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-uw_ct_special_alert_block-block' => array(
          'module' => 'views',
          'delta' => 'uw_ct_special_alert_block-block',
          'region' => 'banner',
          'weight' => '9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('content');
  $export['special_alert'] = $context;

  return $export;
}
