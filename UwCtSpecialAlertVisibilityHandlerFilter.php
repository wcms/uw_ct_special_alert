<?php

/**
 * Promo items filter handler.
 *
 * Filter handler to only show the promo items that should appear for the
 * current page path.
 */
class UwCtSpecialAlertVisibilityHandlerFilter extends views_handler_filter {

  /**
   *
   */
  public function query() {
    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";
    $in_nodes = array();
    $not_in_nodes = array();
    if ($promonodes = node_load_multiple(array(), array('type' => 'uw_special_alert'))) {
      $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
      // Do not display if this is a 404 page.
      if ($path == 'navigation404') {
        // Modify the SQL query to always be false. Tried not running anything
        // at all here, but then the item still displays.
        $this->query->add_where(0, '1 = 2');
      }
      else {
        foreach ($promonodes as $node) {
          // There will not be a value for field_block_visibility if the node
          // form field is empty. In this case, we want to include the promo
          // item in the view.
          if (!empty($node->field_block_visibility)) {
            $pages = trim(drupal_strtolower($node->field_block_visibility['und'][0]['value']));
            if (!empty($pages)) {
              // Split the list of pages into those that start with "not " and
              // those that don't.
              $lines = preg_split('/(\r\n?|\n)/', $pages);
              $pages = '';
              $notpages = '';
              foreach ($lines as $line) {
                // Replace <front> with home page alias.
                $line = str_replace('<front>', drupal_get_path_alias(variable_get('site_frontpage', 'node')), $line);
                if (substr($line, 0, 4) === 'not ') {
                  $notpages .= trim(substr($line, 4)) . "\n";
                }
                else {
                  $pages .= trim($line) . "\n";
                }
              }
              // If there are no $pages, match all paths except $notpages.
              if ($pages === '') {
                $pages = "*\n";
              }
              // Match pages to path to determine whether or not the promo item
              // should be displayed.
              if ($notpages !== '' && drupal_match_path($path, $notpages)) {
                $not_in_nodes[] = $node->nid;
              }
              elseif (drupal_match_path($path, $pages)) {
                $in_nodes[] = $node->nid;
              }
              else {
                $not_in_nodes[] = $node->nid;
              }
            }
          }
          else {
            $in_nodes[] = $node->nid;
          }
        }
        if (!empty($in_nodes)) {
          $this->query->add_where($this->options['group'], 'node.nid', $in_nodes, "IN");
        }
        if (!empty($not_in_nodes)) {
          $this->query->add_where($this->options['group'], 'node.nid', $not_in_nodes, "NOT IN");
        }
      }
    }
  }

}
